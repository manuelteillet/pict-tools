# Pict-tools
[![pipeline status](https://gitlab.com/manuelteillet/pict-tools/badges/master/pipeline.svg)](https://gitlab.com/manuelteillet/pict-tools/commits/master)

A set of small scripts to help me managing my photos.

## Useful commands

Manual rsync:

`rsync --checksum --archive --fuzzy --delay-updates --compress --chown=:http --chmod=775 --progress /media/storage/Photos/Yanis/03-16-yanis-deuxieme-mois/ manu@supernas.dynu.net:/mnt/data/external/manu/Photos/2022/03-16-yanis-deuxieme-mois -v --delete-after`

Video compression:

`for i in 03-19-videos-originales/*;ffmpeg -i $i -c:v libx264 -crf 24 -r 25 -vf yadif,format=yuv420p -s hd720 -preset veryslow -force_key_frames "expr:gte(t,n_forced/2)" -bf 2 -c:a aac -q:a 1 -ac 2 -ar 48000 -use_editlist 0 -movflags +faststart (string replace 03-19-videos-originales/ ./ $i);exiftool -tagsFromFile $i -CreateDate (string replace 03-19-videos-originales/ ./ $i);end`

Thumbnails:

`for dir in 03-16*/;echo $dir;mkdir $dir/pwg_representative;for file in $dir/*.mp4;echo $file;set thumb (string replace $dir $dir/pwg_representative/ (string replace .mp4 .jpg $file));echo $thumb;ffmpegthumbnailer -s512 -f -i$file -o$thumb;end;end`

Gallery links on server:

`for dir in /mnt/data/external/manu/Photos/2022/03-16-yanis-deuxieme-mois/*/; ln -s (string replace /mnt/data/external/manu/Photos/ /external/ $dir) /opt/piwigo/gallery/galleries/;end`

## Installation

Requirements:

* python3.6 >
* exiftool and rsync (3.1.2 >) must be installed and runnable as commands.

Get the [latest version of pict-tools](https://gitlab.com/manuelteillet/pict-tools/tags/v1.0.0) as an archive, untar it and from pict-tools root directory run:

```shell
sudo pip install .
```

Three new commands will then be availiable from your shell:

* pict-rename
* pict-keyword-toggle
* pict-album

## Usage example

Pict-tools works well with gThumb (3.6 >) and a nextcloud server (12.0.3 >).

Here is my process to organize and share pictures with friends:

* Sort the pictures in different folders according to the event that caused the pictures (for example 'travel to India')
* Browse to the 'travel to India' folder from gThumb, select any picture and hit the pict-rename command in gThumb tools menu. It will rename your pictures like this: YYYY-MM-DD-XXXX.jpg where YYYY-MM-DD is the date the picture was taken and XXXX is a four digit counter that sorts the pictures in chronological order. The 'travel to India' folder will be renamed to become 'MM-DD-travel-to-india' where MM-DD is the date of the first picture in the folder.
* Select the pictures to share and hit the 0 key to toggle the 'album' keyword on these pictures.
* Select any picture and hit the pict-album command in gThumb tools menu. It will create a 'MM-DD-travel-to-india-album' sub-folder containing relative links to the album selected pictures and upload everything to the nextcloud server.
* From nextcloud webapp, browse to your external pictures folder. A YYYY folder contains 'MM-DD-travel-to-india', that contains the 'MM-DD-travel-to-india-album' folder that you can share with your friends.

### gThumb configuration

Here is the content of `~/.config/gthumb/scripts.xml`

You can tweak it to fit your needs. You can edit it with a gui from gThumb tools -> personalize.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<scripts version="1.0">
  <script accelerator="" id="VsVi9n9E" shell-script="true" display-name="pict-rename" command="pict-rename %P -o %ask{time offset in +/-H:M }{ 0 } " wait-command="true" for-each-file="false"/>
  <script accelerator="" id="Ke47z3c6" shell-script="true" display-name="create and upload album from album keyword" command="pict-album %P --upload user_name@nextcloud_server_domain_name:/path/to/external/pictures album" wait-command="true" for-each-file="false"/>
  <script accelerator="KP_0" id="NCQ1xN9n" shell-script="true" display-name="album" command="pict-keyword-toggle album %F" wait-command="true" for-each-file="false"/>
  <script accelerator="KP_1" id="k4wqB42W" shell-script="true" display-name="beautiful" command="pict-keyword-toggle beautiful %F" wait-command="true" for-each-file="false"/>
  <script accelerator="KP_2" id="O7P0zR7Q" shell-script="true" display-name="encounters" command="pict-keyword-toggle  encounters %F" wait-command="true" for-each-file="false"/>
  <script accelerator="KP_3" id="U1I3hO1S" shell-script="true" display-name="lol" command="pict-keyword-toggle lol %F" wait-command="true" for-each-file="false"/>
  <script accelerator="KP_4" id="WFKo6mUj" shell-script="true" display-name="friends" command="pict-keyword-toggle friends %F" wait-command="true" for-each-file="false"/>
  <script accelerator="KP_5" id="HNqJ1x9t" shell-script="true" display-name="family" command="pict-keyword-toggle family %F" wait-command="true" for-each-file="false"/>
</scripts>
```

To avoid irritating `.comment` folders, you have to set "store metadata inside files if possible" and disable the "comments and tags" extension in gThumb preferences.

### Nextcloud configuration

I'm running nextcloud in a docker using the collabora configuration found [here](https://github.com/SnowMB/nextcloud).

You must be able to ssh into your nextcloud server with a rsa key (no password) from the machine you are using pict-tools on.

On your nextcloud server your user must be in the http group so nextcloud get correct rights on the files pushed by the pict-album command.

You must use an 'external storage' on your nextcloud server as target for pict-album command so nextcloud gets its database updated automatically when new pictures are pushed. To do so, add a volume to your nextcloud docker with a line in your `docker-compose.yml` looking like that:

```yaml
- /mnt/data/external:/var/www/external
```

Then create a new 'external storage' in your nextcloud admin panel (external storage is provided by an app) and make it point to `/var/www/external/user_name`. Be careful if you use the official nextcloud docker as I do, mounting your external data to any subfolder of `/var/www/html/` will result in a wipe of your external data on docker stop ! :-(

## Detailed usage

### pict-rename

```shell
Rename pictures and videos according to their creation date.

Usage:
  pict-rename <files_directory> [options]

Arguments:
  files_directory             directory containing files to rename

Options:
  -h --help                   show this screen
  --version                   show version
  -d --dry-run                preview only (no actual renaming)
  -o <time>, --offset <time>  time offset to apply (format : +/-H:M)
```

pict-rename works also with most video files.

The offset option modify the 'CreateDate' of the pictures (and rename them accordingly).

You can add some pictures to an already renamed folder, the new pictures will merge with the existing pictures nicely without overwriting any picture.

So if you have two cameras for shooting the same event and the cameras dates dosent match, you can rename the pictures from the camera with the wrong date first adding an offset and then add the good ones and rename again.

### pict-keyword-toggle

```shell
Toggles keywords in the 'keywords' exif tag.

Usage:
  pict-keyword-toggle <keyword_to_toggle> <files_to_edit>...

Arguments:
  keyword_to_toggle  well...keyword to toggle
  files_to_edit      files list to apply keyword toggle

Options:
  -h --help          show this screen
  --version          show version
```

gThumb use the 'Keywords' as tags and you can filter or search in your pictures collection thanks to these tags.

### pict-album

```shell
Create album from keyword in given picture directory.

Usage:
  pict-album <pictures_directory> [options] [<album_keyword>]

Arguments:
  <pictures_directory>        directory containing the
                              pictures to make an album from.
  <album_keyword>             keyword used to select the album
                              pictures [default: album].

Options:
  -h, --help                  show this screen.
  --version                   show version.
  -u <dest>, --upload <dest>  upload directory plus album to sftp
                              destination in matching year directory.
  -i <key>, --id <key>        path to specific id-rsa key
  -p, --preserve              preserve links in destination album directory
                              even if images are removed from album
  -d, --delete                delete files on destination if not found
                              in pictures directory.
```

If you run the command more than one time, the default behavior is to not delete pictures already uploaded in nextcloud if they are missing in your local directory, but if some links in the album folder are deleted, they will be deleted in nextcloud. It is possible to change this behavior with the --preserve and --delete options.

## Run tests

As every great piece of software, pict-tools are fully tested. To run the tests on your machine, you must have installed and accessible as commands ssh_agent, ssh_keyscan, ssh_keygen and docker.

To run the full test suite, get the [latest version of pict-tools](https://gitlab.com/manuelteillet/pict-tools/tags/v1.0.0) as an archive, untar it and from pict-tools root directory run:

```shell
sudo pip install .[test]
tox
```