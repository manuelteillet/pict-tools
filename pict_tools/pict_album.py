#!/usr/bin/env python3

"""Create album from keyword in given picture directory.

Usage:
  pict-album <pictures_directory> [options] [<album_keyword>]

Arguments:
  <pictures_directory>  directory containing the
                        pictures to make an album from.
  <album_keyword>       keyword used to select the album
                        pictures [default: album].

Options:
  -h, --help            show this screen.
  --version             show version.
  -r, --recursive       recursively create albums for all subdirectories containing pictures
"""

from shutil import rmtree
from docopt import docopt
from pict_tools.exiftool import ExifTool
import pkg_resources
from glob import glob
from os import path, walk, makedirs, symlink


def main():
    version_string = (
        f"pict-album from pict-tools "
        f'{pkg_resources.require("pict-tools")[0].version}'
    )
    arguments = docopt(__doc__, version=version_string)
    pictures_directory = path.abspath(arguments["<pictures_directory>"])
    album_keyword = arguments["<album_keyword>"] or "album"
    recursive = arguments["--recursive"]

    if recursive:
        pictures_directories = sorted(
            [x[0] for x in walk(pictures_directory)], reverse=True
        )
    else:
        pictures_directories = [pictures_directory]

    for pictures_directory in pictures_directories:
        pict_album_for_directory(pictures_directory, album_keyword)


def pict_album_for_directory(pictures_directory, album_keyword):
    file_list = [
        file_name
        for file_name in glob(f"{pictures_directory}/*.*")
        if not path.islink(file_name)
    ]

    if not file_list:
        return

    album_directory = path.join(
        pictures_directory, f"{path.basename(pictures_directory)}-{album_keyword}"
    )

    if path.exists(album_directory):
        rmtree(album_directory)
    makedirs(album_directory)

    with ExifTool() as et:
        for file in file_list:
            keywords_tag = et.get_tag("Keywords", file) or ""
            if album_keyword in keywords_tag:
                symlink(
                    path.join("..", path.basename(file)),
                    path.join(album_directory, path.basename(file)),
                )

    file_list = glob(f"{album_directory}/*.*")

    if not file_list:
        rmtree(album_directory)


if __name__ == "__main__":
    main()
