#!/usr/bin/env python3

"""Create piwigo album from keyword.

Usage:
  pict-album-server <pictures_directory> <album_base_directory> <album_keyword> [options] 

Arguments:
  <pictures_directory>      directory containing the
                            pictures to make an album from.
  <album_base_directory>    base directory where the album links 
                            will be created.
  <album_keyword>           keyword used to select the album
                            pictures.
Options:
  -h, --help            show this screen.
  --version             show version.
"""

from shutil import rmtree
from sh import ffmpegthumbnailer
from docopt import docopt
from pict_tools.exiftool import ExifTool
import pkg_resources
from glob import glob
from os import path, walk, makedirs, symlink


def main():
    version_string = (
        f"pict-album-server from pict-tools "
        f'{pkg_resources.require("pict-tools")[0].version}'
    )
    arguments = docopt(__doc__, version=version_string)
    pictures_directory = path.abspath(arguments["<pictures_directory>"])
    album_base_directory = path.abspath(arguments["<album_base_directory>"])
    album_keyword = arguments["<album_keyword>"]

    pict_album_server(pictures_directory, album_base_directory, album_keyword)


def pict_album_server(pictures_directory, album_base_directory, album_keyword):
    file_list = [
        file_name
        for file_name in glob(f"{pictures_directory}/*.*")
        if not path.islink(file_name)
    ]

    if not file_list:
        return

    album_directory = path.join(
        album_base_directory, f"{path.basename(pictures_directory)}-{album_keyword}"
    )

    print(album_directory)
    if path.exists(album_directory):
        rmtree(album_directory)
    makedirs(album_directory)

    with ExifTool() as et:
        for file in file_list:
            keywords_tag = et.get_tag("Keywords", file) or ""
            if album_keyword in keywords_tag:
                print(path.join(album_directory, path.basename(file)), "->", file)

                symlink(
                    file,
                    path.join(album_directory, path.basename(file)),
                )

                if file.lower().endswith(".mp4"):
                    video_thumbnails_dir = path.join(
                        album_directory, "pwg_representative"
                    )
                    if not path.exists(video_thumbnails_dir):
                        makedirs(video_thumbnails_dir)
                    ffmpegthumbnailer(
                        "-s512",
                        "-f",
                        f"-i{file}",
                        f"-o{path.join(video_thumbnails_dir, path.basename(file).replace('.mp4', '.jpg'))}",
                        _err_to_out=True,
                    )

    file_list = glob(f"{album_directory}/*.*")

    if not file_list:
        rmtree(album_directory)


if __name__ == "__main__":
    main()
