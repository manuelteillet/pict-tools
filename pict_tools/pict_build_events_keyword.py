#!/usr/bin/env python3

"""Build new events special keyword according to directory name.

Usage:
  pict-build-events-keyword <files_directory> [options]

Arguments:
  files_directory             directory containing files to build events keyword for

Options:
  -h, --help                  show this screen
  --version                   show version
  -r, --recursive             recursively build events keyword in all subdirectories containing pictures
  -d, --dry-run               preview only (no actual keyword set)
"""

from docopt import docopt
from termcolor import colored
from glob import glob
from pict_tools.exiftool import ExifTool
from pict_tools.tools import dotdict
from pict_tools.pict_keyword_toggle import (
    check_and_set_keyword,
    check_pattern_and_set_keyword,
)

from os import path, renames, walk, makedirs, remove, sep
from importlib import metadata
import re
from PIL import Image
import hashlib
from itertools import combinations
from bisect import insort
import sys
from pathlib import Path


def main():
    version_string = (
        f'pict-build-events-keyword from pict-tools {metadata.version("pict-tools")}'
    )
    arguments = docopt(__doc__, version=version_string)
    (files_directory, options) = parse_arguments(arguments)
    _pict_build_events_keyword(files_directory, options)


def _pict_build_events_keyword(files_directory, options={}):
    files_list = build_files_list(files_directory, options)

    if files_list == []:
        print("No files found for renaming.")
        return

    for file in files_list:
        if not check_pattern_and_set_keyword(
            file, "events:", _build_events_from_filename(file)
        ):
            print(f"events already set for {file}")


def parse_arguments(arguments):
    options = dotdict(
        {
            key[2:].replace("-", "_"): value
            for key, value in arguments.items()
            if key.startswith("--")
        }
    )

    return (files_directory, options)


def build_files_list(files_directory, options):
    return sorted(
        [
            Path(file_name)
            for file_name in glob(
                f"{files_directory}{'/**' if options.recursive else ''}/*.*",
                recursive=True,
            )
            if not path.islink(file_name)
        ]
    )


def _build_events_from_filename(file):
    year_matcher = re.compile("^\d\d\d\d$")
    years_as_list = list(filter(year_matcher.match, file.parts))
    if len(years_as_list) == 1:
        events_folder_name = file.parts[file.parts.index(years_as_list[0]) + 1]
        clean_events = re.sub(
            "[-' ]",
            "_",
            re.sub(
                "^\d\d[-_ ]?\d\d[-_ ]?|[-_ ]?\d\d[-_ ]?\d\d$", "", events_folder_name
            ).lower(),
        )
        return f"events:{clean_events}"
    return "events:toto"


def build_files_info_list(files_list):
    with ExifTool() as et:
        keyword_list = et.get_tag_batch("Keywords", files_list)

    return zip(files_list, keyword_list)
