#!/usr/bin/env python3

"""Toggles keywords in the 'keywords' exif tag.

Usage:
  pict-keyword-toggle <keyword_to_toggle> <files_to_edit>...

Arguments:
  keyword_to_toggle  well...keyword to toggle
  files_to_edit      files list to apply keyword toggle

Options:
  -h, --help         show this screen
  --version          show version
"""

from docopt import docopt
from pict_tools.exiftool import ExifTool
import pyexiv2
from importlib import metadata
from os import path


def main():
    version_string = (
        f'pict-keyword-toggle from pict-tools {metadata.version("pict-tools")}'
    )
    arguments = docopt(__doc__, version=version_string)
    keyword_to_toggle = arguments["<keyword_to_toggle>"]
    files_to_edit = arguments["<files_to_edit>"]
    _keyword_toggle(files_to_edit, keyword_to_toggle)


def _keyword_toggle(files_to_edit, keyword_to_toggle):
    for file_to_edit in files_to_edit:
        if path.splitext(file_to_edit)[1] == ".mp4":
            with ExifTool() as et:
                if _is_keyword_in_file(file_to_edit, keyword_to_toggle, et):
                    _unset_keyword(file_to_edit, keyword_to_toggle, et)
                else:
                    _set_keyword(file_to_edit, keyword_to_toggle, et)
        else:
            metadata = pyexiv2.Image(file_to_edit)
            if _is_keyword_in_file(file_to_edit, keyword_to_toggle, metadata):
                _unset_keyword(file_to_edit, keyword_to_toggle, metadata)
            else:
                _set_keyword(file_to_edit, keyword_to_toggle, metadata)


def _is_keyword_in_file(file_path, keyword, handler):
    if path.splitext(file_path)[1] == ".mp4":
        keywords_tag = handler.get_tag("Keywords", file_path)
    else:
        iptc_tags = handler.read_iptc()
        xmp_tags = handler.read_xmp()

        if "Iptc.Application2.Keywords" not in iptc_tags:
            iptc_tags["Iptc.Application2.Keywords"] = []
            xmp_tags["Xmp.iptc.Keywords"] = []
        keywords_tag = iptc_tags["Iptc.Application2.Keywords"]
    return keywords_tag and keyword in keywords_tag


def _set_keyword(file_path, keyword, handler):
    if path.splitext(file_path)[1] == ".mp4":
        keywords_tag = handler.get_tag("Keywords", file_path)
        keywords_tag = [] if not keywords_tag else keywords_tag.split(", ")
        keywords_tag.append(keyword)
        handler.set_tag(f'Keywords={", ".join(keywords_tag)}', file_path)
    else:
        iptc_tags = handler.read_iptc()
        xmp_tags = handler.read_xmp()
        if "Iptc.Application2.Keywords" not in iptc_tags:
            iptc_tags["Iptc.Application2.Keywords"] = []
            xmp_tags["Xmp.iptc.Keywords"] = []
        iptc_tags["Iptc.Application2.Keywords"].append(keyword)
        xmp_tags["Xmp.iptc.Keywords"].append(keyword)
        handler.modify_iptc(iptc_tags)
        handler.modify_xmp(xmp_tags)


def _unset_keyword(file_path, keyword, handler):
    # TODO: check handler instead of file extension
    if path.splitext(file_path)[1] == ".mp4":
        keywords_tag = handler.get_tag("Keywords", file_path)
        keywords_tag = [] if not keywords_tag else keywords_tag.split(", ")
        keywords_tag.remove(keyword)
        handler.set_tag(f'Keywords={", ".join(keywords_tag)}', file_path)
    else:
        iptc_tags = handler.read_iptc()
        xmp_tags = handler.read_xmp()
        if "Iptc.Application2.Keywords" not in iptc_tags:
            iptc_tags["Iptc.Application2.Keywords"] = []
            xmp_tags["Xmp.iptc.Keywords"] = []
        iptc_tags["Iptc.Application2.Keywords"].remove(keyword)
        xmp_tags["Xmp.iptc.Keywords"].remove(keyword)
        handler.modify_iptc(iptc_tags)
        handler.modify_xmp(xmp_tags)


def check_and_set_keyword(file_path, keyword):
    if path.splitext(file_path)[1] == ".mp4":
        with ExifTool() as et:
            if not _is_keyword_in_file(file_path, keyword, et):
                _set_keyword(file_path, keyword, et)
    else:
        metadata = pyexiv2.Image(file_path)
        if not _is_keyword_in_file(file_path, keyword, metadata):
            _set_keyword(file_path, keyword, metadata)


def check_and_unset_keyword(file_path, keyword):
    if path.splitext(file_path)[1] == ".mp4":
        with ExifTool() as et:
            if _is_keyword_in_file(file_path, keyword, et):
                _unset_keyword(file_path, keyword, et)
    else:
        metadata = pyexiv2.Image(file_path)
        if _is_keyword_in_file(file_path, keyword, metadata):
            _unset_keyword(file_path, keyword, metadata)


# for file_to_edit in files_to_edit:
#     if path.splitext(file_to_edit)[1] == ".mp4":
#         with ExifTool() as et:
#             keywords_tag = et.get_tag("Keywords", file_to_edit)
#             keywords_tag = [] if not keywords_tag else keywords_tag.split(", ")
#             if keyword_to_toggle in keywords_tag:
#                 keywords_tag.remove(keyword_to_toggle)
#             else:
#                 keywords_tag.append(keyword_to_toggle)
#             et.set_tag(f'Keywords={", ".join(keywords_tag)}', file_to_edit)
#     else:
#         metadata = pyexiv2.Image(file_to_edit)
#         iptc_tags = metadata.read_iptc()
#         xmp_tags = metadata.read_xmp()
#         print(iptc_tags)
#         print(xmp_tags)
#         if "Iptc.Application2.Keywords" not in iptc_tags:
#             iptc_tags["Iptc.Application2.Keywords"] = []
#             xmp_tags["Xmp.iptc.Keywords"] = []
#         keywords_tag = iptc_tags["Iptc.Application2.Keywords"]
#         if keyword_to_toggle in keywords_tag:
#             iptc_tags["Iptc.Application2.Keywords"].remove(keyword_to_toggle)
#             xmp_tags["Xmp.iptc.Keywords"].remove(keyword_to_toggle)
#         else:
#             iptc_tags["Iptc.Application2.Keywords"].append(keyword_to_toggle)
#             xmp_tags["Xmp.iptc.Keywords"].append(keyword_to_toggle)
#         metadata.modify_iptc(iptc_tags)
#         metadata.modify_xmp(xmp_tags)


if __name__ == "__main__":
    main()
