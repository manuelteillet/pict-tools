#!/usr/bin/env python3

"""Rename pictures and videos according to their creation date.

Usage:
  pict-rename <files_directory> [options]

Arguments:
  files_directory                directory containing files to rename
   
Options:   
  -h, --help                     show this screen
  --version                      show version
  -d, --dry-run                  preview only (no actual renaming)
  -o <time>, --offset <time>     time offset to apply (format : +/-H:M or +/-H)
  -m <dir>, --move <dir>         move renamed files to given root directory and organize in year and month subdirectories
  -r, --recursive                recursively rename all subdirectories containing pictures
  --no-delete                    do not delete exact duplicates
  --fill-camera-id <str>         fill empty camera ids with str
  --override-camera-id <str>     override camera ids with str  
"""

from docopt import docopt
from termcolor import colored
from glob import glob
from pict_tools.exiftool import ExifTool
from pict_tools.tools import dotdict
from pict_tools.pict_keyword_toggle import check_and_set_keyword

from os import path, renames, walk, makedirs, remove, sep
from importlib import metadata
import re
from PIL import Image
import hashlib
from itertools import combinations
from bisect import insort
import sys


class RenameError(Exception):
    def __init__(self, message):
        sys.exit(colored(message, "red"))


def main():
    version_string = f'pict-rename from pict-tools {metadata.version("pict-tools")}'
    arguments = docopt(__doc__, version=version_string)
    (files_directory, options) = parse_arguments(arguments)
    _pict_rename(files_directory, options)


def _pict_rename(files_directory, options={}):
    files_list = build_files_list(files_directory, options)

    if files_list == []:
        print("No files found for renaming.")
        return

    cleanup_xmp(files_list)

    # TODO: use offset in files_dict new_name, and do not forget to apply it to exif create date in print_and_rename
    if options.offset:
        apply_offset(files_list, options.offset)

    files_dict = build_files_dict(files_list, options)

    print_and_rename(files_dict, options)

    if options.offset and options.dry_run:
        offset_back = "-" if options.offset[0] == "+" else "+"
        offset_back += options.offset[1:]
        apply_offset(files_list, offset_back)


def parse_arguments(arguments):
    files_directory = path.abspath(arguments["<files_directory>"])
    offset = arguments["--offset"]
    recursive = arguments["--recursive"]
    move = arguments["--move"]
    move = path.abspath(move) if move is not None else None

    if not path.isdir(files_directory):
        raise RenameError("files_directory is not a valid directory")
    if move and files_directory + sep in move + sep and recursive:
        raise RenameError("recursive move to a sub-directory is impossible")
    if offset and re.match("[+-]\d+($|:\d+$)", offset) == None:
        raise RenameError("invalid offset format")

    options = dotdict(
        {
            key[2:].replace("-", "_"): value
            for key, value in arguments.items()
            if key.startswith("--")
        }
    )

    return (files_directory, options)


def build_files_list(files_directory, options):
    return sorted(
        [
            file_name
            for file_name in glob(
                f"{files_directory}{'/**' if options.recursive else ''}/*.*",
                recursive=True,
            )
            if not path.islink(file_name)
        ]
    )


def cleanup_xmp(files_list):
    with ExifTool() as et:
        dirty_list = et.get_tag_batch("XMP-Container:all", files_list)
    for dirty_file in zip(files_list, dirty_list):
        if dirty_file[1] != None:
            print("cleaning", dirty_file[0])
            with ExifTool() as et:
                et.set_tag("XMP-Container:all=", dirty_file[0])


def apply_offset(files_list, offset):
    with ExifTool() as et:
        for file_name in files_list:
            result = et.set_tag(f"CreateDate{offset[0]}={offset[1:]}", file_name)
            if "1 image files updated" not in result:
                print(colored(f"Cannot apply offset to {file_name}", "yellow"))


def build_files_dict(files_list, options):
    files_dict = {}
    # TODO:use dotdict

    files_info_list = build_files_info_list(files_list)

    for file_name, create_date, camera_brand, camera_model in files_info_list:
        files_dict[file_name] = {"file_name": file_name}
        file_info = files_dict[file_name]

        add_create_date(file_info, create_date)
        add_camera_id(file_info, camera_brand, camera_model, options)

    destination_dict = {}
    # TODO:use dotdict
    if options.move:
        build_destination_dict(destination_dict, files_dict, options)

    add_duplicates(files_dict, destination_dict)

    duplicate_create_date_files_groups_list = (
        build_duplicate_create_date_files_groups_list(destination_dict | files_dict)
    )
    # TODO:duplicate_create_date_files_groups_list already created in add_duplicates

    if not options.no_delete:
        remove_deleted_from_duplicates_list(
            files_dict, duplicate_create_date_files_groups_list
        )

    add_new_file_names(
        files_dict, duplicate_create_date_files_groups_list, destination_dict, options
    )

    return files_dict


def remove_deleted_from_duplicates_list(
    files_dict, duplicate_create_date_files_groups_list
):
    for file_name, file_info in files_dict.items():
        if "exact_duplicate_of" in file_info:
            for duplicate_file_name in file_info["exact_duplicate_of"]:
                if duplicate_file_name in files_dict:
                    files_dict[duplicate_file_name] = "to_delete"
                for (
                    duplicate_create_date_files_group
                ) in duplicate_create_date_files_groups_list:
                    if duplicate_file_name in duplicate_create_date_files_group:
                        duplicate_create_date_files_group.remove(duplicate_file_name)


def add_new_file_names(
    files_dict, duplicate_create_date_files_groups_list, destination_dict, options
):
    for file_name, file_info in files_dict.items():
        if file_info == "to_delete":
            continue
        new_file_name = build_new_file_name(file_name, file_info, options, index=0)
        for (
            duplicate_create_date_files_group
        ) in duplicate_create_date_files_groups_list:
            if file_name in duplicate_create_date_files_group:
                print(duplicate_create_date_files_group)
                new_file_name = build_new_file_name(
                    file_name,
                    file_info,
                    options,
                    duplicate_create_date_files_group.index(file_name),
                )
        if not options.move and new_file_name in files_dict.keys():
            file_info["tmp_file_name"] = build_tmp_file_name(new_file_name)
        if options.move and new_file_name in destination_dict.keys():
            is_duplicate = add_duplicate(files_dict, file_name, new_file_name)
            if not (not options.no_delete and is_duplicate == "exact_duplicate"):
                directory = path.dirname(new_file_name)
                full_basename = path.basename(new_file_name)
                basename = path.splitext(full_basename)[0]
                extension = path.splitext(full_basename)[1]
                new_file_name = path.join(directory, f"{basename}_dup{extension}")
                print_warning(
                    f"{full_basename} already present in destination directory with a naming issue (wrong creation date ?). {file_name} will be renamed {new_file_name}."
                )
        file_info["new_file_name"] = new_file_name


def add_create_date(file_info, create_date):
    if not is_create_date_valid(create_date):
        rebuilt_create_date = build_create_date_from_file_name(file_info["file_name"])
        if is_create_date_valid(rebuilt_create_date):
            file_info["create_date"] = rebuilt_create_date
            file_info["need_write_create_date"] = True
        else:
            file_info["create_date"] = ""
    else:
        file_info["create_date"] = create_date


def add_camera_id(file_info, camera_brand, camera_model, options):
    file_info["camera_id"] = (
        options.override_camera_id
        if options.override_camera_id
        else build_camera_id(camera_brand, camera_model)
        if camera_brand or camera_model
        else options.fill_camera_id
        if options.fill_camera_id
        else ""
    )


def build_files_info_list(files_list):
    with ExifTool() as et:
        create_dates_list = et.get_tag_batch("CreateDate", files_list)
        camera_brands_list = et.get_tag_batch("Make", files_list)
        camera_models_list = et.get_tag_batch("Model", files_list)

    return zip(files_list, create_dates_list, camera_brands_list, camera_models_list)


def is_create_date_valid(create_date):
    return (
        create_date != None
        and create_date != "0000:00:00 00:00:00"
        and re.match("\d{4}:\d{2}:\d{2} \d{2}:\d{2}:\d{2}", create_date) != None
    )


def build_create_date_from_file_name(file_name):
    return (
        path.splitext(path.basename(file_name))[0]
        .replace("-", ":")
        .replace("_", " ")[:19]
    )


def build_camera_id(camera_brand, camera_model):
    formatted_camera_brand = (
        camera_brand.lower().replace(" ", "-") if camera_brand else ""
    )
    formatted_camera_model = (
        camera_model.lower().replace(" ", "-") if camera_model else ""
    )
    camera_id = (
        formatted_camera_brand
        if camera_brand and not formatted_camera_brand in formatted_camera_model
        else ""
    )
    camera_id += "-" if camera_id and camera_model else ""
    camera_id += formatted_camera_model if camera_model else ""
    return camera_id


def add_duplicates(files_dict, destination_dict):
    for file_group in build_duplicate_create_date_files_groups_list(
        files_dict | destination_dict
    ):
        for file_1, file_2 in combinations(file_group, 2):
            add_duplicate(files_dict, file_1, file_2)


def build_destination_dict(destination_dict, files_dict, options):
    move_dirs_set = set()
    for file_name, file_info in files_dict.items():
        if not "create_date" in file_info:
            continue
        create_date = file_info["create_date"]
        move_dir = build_destination_directory(options, create_date)
        move_dirs_set.add(move_dir)
    for move_dir in move_dirs_set:
        files_list = glob(f"{move_dir}/*.*")
        if not files_list:
            continue
        for file_name, create_date in build_create_dates_list(files_list):
            destination_dict[file_name] = {}
            destination_dict[file_name]["create_date"] = create_date
            # TODO:use setdefault


def build_create_dates_list(files_list):
    with ExifTool() as et:
        create_dates_list = et.get_tag_batch("CreateDate", files_list)
    return zip(files_list, create_dates_list)


def build_duplicate_create_date_files_groups_list(dict_to_search):
    rev_dict = {}
    for file_name, file_info in (dict_to_search).items():
        rev_dict.setdefault(file_info["create_date"], []).append(file_name)

    return list(filter(lambda x: len(x) > 1, rev_dict.values()))


def add_duplicate(files_dict, file_1, file_2):
    if file_hash(file_1) == file_hash(file_2):
        if file_1 in files_dict:
            files_dict[file_1].setdefault("exact_duplicate_of", []).append(file_2)
        elif file_2 in files_dict:
            files_dict[file_2].setdefault("exact_duplicate_of", []).append(file_1)
        return "exact_duplicate"
    elif image_hash(file_1) == image_hash(file_2):
        if file_1 in files_dict:
            files_dict[file_1].setdefault("duplicate_except_tags_of", []).append(file_2)
        elif file_2 in files_dict:
            files_dict[file_2].setdefault("duplicate_except_tags_of", []).append(file_1)
        return "duplicate_except_tags"


def file_hash(file_path):
    file = open(file_path, "rb")
    return hashlib.sha512(file.read()).hexdigest()


def image_hash(image_path):
    im = Image.open(image_path)
    return hashlib.sha512(im.tobytes()).hexdigest()


def print_and_rename(files_dict, options):
    for file_name, file_info in files_dict.items():
        if file_info == "to_delete":
            continue
        if file_info["create_date"] == "":
            print_error(
                f"Impossible to rename file {file_name} because of bad create date."
            )
            continue
        if "need_write_create_date" in file_info:
            print_warning(f"Fixing create date on {file_info['new_file_name']}")
            with ExifTool() as et:
                et.set_tag(f"CreateDate={file_info['create_date']}", file_name)
        if "exact_duplicate_of" in file_info:
            for exact_duplicate in file_info["exact_duplicate_of"]:
                if options.no_delete:
                    print_warning(
                        f"{files_dict[exact_duplicate]['new_file_name'] if exact_duplicate in files_dict else exact_duplicate} is an exact duplicate of {file_info['new_file_name']}."
                    )
                else:
                    print_delete(
                        f"{exact_duplicate} deleted as exact duplicate of {file_info['new_file_name']}."
                    )
                    if not options.dry_run:
                        remove(exact_duplicate)
        if "duplicate_except_tags_of" in file_info:
            for duplicate_except_tags in file_info["duplicate_except_tags_of"]:
                if not (
                    duplicate_except_tags in files_dict
                    and files_dict[duplicate_except_tags] == "to_delete"
                ):
                    # TODO:mix keyword tags
                    print_warning(
                        f"{files_dict[duplicate_except_tags]['new_file_name'] if duplicate_except_tags in files_dict else duplicate_except_tags} is a duplicate except for tags of {file_info['new_file_name']}."
                    )
        print_rename(file_name, file_info["new_file_name"])
        if not options.dry_run:
            if "camera_id" in file_info and file_info["camera_id"]:
                check_and_set_keyword(file_name, f"camera:{file_info['camera_id']}")
            if "tmp_file_name" in file_info:
                renames(file_name, file_info["tmp_file_name"])
            else:
                renames(file_name, file_info["new_file_name"])

    if not options.dry_run:
        for file_info in files_dict.values():
            if "tmp_file_name" in file_info:
                renames(file_info["tmp_file_name"], file_info["new_file_name"])


def build_new_file_name(file_name, file_info, options, index):
    directory = (
        path.dirname(file_name)
        if not options.move
        else build_destination_directory(options, file_info["create_date"])
    )
    full_basename = path.basename(file_name)
    basename = path.splitext(full_basename)[0]
    extension = path.splitext(full_basename)[1]
    new_name_date = f'{file_info["create_date"].replace(":", "-").replace(" ","_")}'
    new_name_camera_id = (
        f'_{file_info["camera_id"]}' if file_info["camera_id"] != "" else ""
    )
    new_name = f"{new_name_date}_{index:02d}{new_name_camera_id}"
    if new_name_date in basename:
        common_length = len(
            re.findall(f"({new_name_date}([-_]\d+)?({new_name_camera_id})?)", basename)[
                0
            ][0]
        )
        new_name = f"{new_name}{basename[common_length:]}"
        if new_name.endswith("_dup"):
            new_name = new_name[:-4]
    return path.join(directory, f"{new_name}{extension.lower()}")


def print_error(message):
    print(colored(f"ERROR: {message}", "red"))


def print_warning(message):
    print(colored(f"WARNING: {message}", "yellow"))


def print_delete(message):
    print(colored(f"INFO: {message}", "red"))


def print_rename(file_name, new_name):
    if file_name == new_name:
        print(f"INFO: {file_name} -> {new_name}")
    else:
        print(
            colored(
                f"INFO: {file_name} -> {new_name}",
                "blue",
            )
        )


def build_destination_directory(options, create_date):
    year = create_date[:4]
    month = create_date[5:7]
    return path.join(options.move, year, month)


def build_tmp_file_name(new_file_name):
    directory = path.dirname(new_file_name)
    new_name = path.basename(new_file_name)
    return path.join(directory, f"tmp_{new_name}")


if __name__ == "__main__":
    main()
