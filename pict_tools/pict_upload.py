#!/usr/bin/env python3

"""Upload directory to a given remote or local destination.

Usage:
  pict-upload <pictures_directory> <destination> [options]

Arguments:
  <pictures_directory>        directory containing the
                              pictures to upload.
  <destination>               destination directory to upload the pictures directory to.
                              pictures directory will be uploaded in a matching year directory.

Options:
  -h, --help                  show this screen.
  --version                   show version.
  -k, --keep-album-links      keep links in destination albums
                              even if images are removed from album
  -d, --delete-pictures       delete picture files on destination if not found
                              in picture directories.
  -r, --recursive             recursively upload all subdirectories containing pictures
                              (albums are uploaded as well if present)

"""

from docopt import docopt
from pict_tools.exiftool import ExifTool
import pkg_resources
from glob import glob
from os import path, walk, makedirs
from sh import rsync, ssh


def main():
    version_string = (
        f"pict-tools from pict-tools "
        f'{pkg_resources.require("pict-tools")[0].version}'
    )
    arguments = docopt(__doc__, version=version_string)
    pictures_directory = path.abspath(arguments["<pictures_directory>"])
    destination = arguments["<destination>"]
    keep_album_links = arguments["--keep-album-links"]
    delete_pictures = arguments["--delete-pictures"]
    recursive = arguments["--recursive"]

    if recursive:
        pictures_directories = sorted(
            [x[0] for x in walk(pictures_directory)], reverse=True
        )
    else:
        pictures_directories = [pictures_directory]

    for pictures_directory in pictures_directories:
        pict_upload_directory(
            pictures_directory, destination, keep_album_links, delete_pictures
        )


def pict_upload_directory(
    pictures_directory, destination, keep_album_links, delete_pictures
):
    file_list = [
        file_name
        for file_name in glob(f"{pictures_directory}/*.*")
        if not path.islink(file_name)
    ]

    if not file_list:
        return

    album_directory = path.join(
        pictures_directory, f"{path.basename(pictures_directory)}-album"
    )

    with ExifTool() as et:
        directory_year = et.get_tag("CreateDate", sorted(file_list)[0])[:4]

    if ":" not in destination and not path.exists(
        path.join(destination, directory_year)
    ):
        makedirs(path.join(destination, directory_year))

    options = [
        "--checksum",
        "--archive",
        "--fuzzy",
        "--delay-updates",
        "--compress",
        "--chown=:http",
        "--chmod=775",
        "--progress",
        f"--exclude={path.basename(album_directory)}/",
    ]
    if delete_pictures:
        options.append("--delete-after")
    print(
        rsync(
            *options,
            pictures_directory,
            path.join(destination, directory_year),
            _err_to_out=True,
        )
    )

    if path.exists(album_directory):
        options = [
            "--checksum",
            "--archive",
            "--fuzzy",
            "--delay-updates",
            "--compress",
            "--chown=:http",
            "--chmod=775",
            "--progress",
        ]
        if not keep_album_links:
            options.append("--delete-after")
        print(
            rsync(
                *options,
                album_directory,
                path.join(
                    destination, directory_year, path.basename(pictures_directory)
                ),
                _err_to_out=True,
            )
        )

    if ":" in destination:
        destination_server = destination.split(":")[0]
        destination_path = destination.split(":")[1]

        print(
            ssh(
                destination_server,
                f"chgrp http {path.join(destination_path, directory_year)};"
                f"chmod 775 {path.join(destination_path, directory_year)}",
                _err_to_out=True,
            )
        )


if __name__ == "__main__":
    main()
