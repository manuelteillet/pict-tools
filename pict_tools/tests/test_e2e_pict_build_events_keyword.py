from pict_tools.pict_build_events_keyword import _pict_build_events_keyword
import pict_tools
from pict_tools.pict_keyword_toggle import check_and_set_keyword
from pict_tools.tools import dotdict
from pict_tools.exiftool import ExifTool
import tarfile

import pytest
from shutil import rmtree, move, copytree, copyfile
from sh import pict_rename, pict_album
from glob import glob
from termcolor import colored
from tempfile import mkdtemp
from pathlib import PurePath
import pyexiv2
from pathlib import Path
from docopt import docopt

tmp_dir = Path(mkdtemp())
test_picts_dir = tmp_dir / "pict_tools_test"
test_dest_dir = tmp_dir / "pict_tools_test_destination"
test_file_1 = test_picts_dir / "test_file_1.jpg"
test_file_2 = test_picts_dir / "test_file_2.jpg"


@pytest.fixture()
def init_and_cleanup_2_test_pictures():
    untar_test_pictures()
    with ExifTool() as et:
        et.set_tag("CreateDate=2000:01:01 12:00:00", str(test_file_1))
        et.set_tag("Make=Google", str(test_file_1))
        et.set_tag("Model=Pixel 5", str(test_file_1))
        et.set_tag("CreateDate=2000:01:01 13:00:00", str(test_file_2))
        et.set_tag("Make=Canon", str(test_file_2))
        et.set_tag("Model=750D", str(test_file_2))

    yield ()

    rmtree(tmp_dir, ignore_errors=True)


@pytest.fixture()
def init_and_cleanup_1_test_picture():
    untar_test_pictures()
    with ExifTool() as et:
        et.set_tag("CreateDate=2000:01:01 12:00:00", str(test_file_1))
        et.set_tag("Make=Google", str(test_file_1))
        et.set_tag("Model=Pixel 5", str(test_file_1))
    test_file_2.unlink()

    yield ()

    rmtree(tmp_dir, ignore_errors=True)


@pytest.fixture()
def init_and_cleanup_2_test_pictures_without_camera_id():
    untar_test_pictures()
    with ExifTool() as et:
        et.set_tag("CreateDate=2000:01:01 12:00:00", str(test_file_1))
        et.set_tag("CreateDate=2000:01:01 13:00:00", str(test_file_2))

    yield ()

    rmtree(tmp_dir, ignore_errors=True)


@pytest.fixture()
def init_and_cleanup_2_test_pictures_with_mixed_camera_ids():
    untar_test_pictures()
    with ExifTool() as et:
        et.set_tag("CreateDate=2000:01:01 12:00:00", str(test_file_1))
        et.set_tag("Make=Google", str(test_file_1))
        et.set_tag("Model=Pixel 5", str(test_file_1))
        et.set_tag("CreateDate=2000:01:01 13:00:00", str(test_file_2))

    yield ()

    rmtree(tmp_dir, ignore_errors=True)


def untar_test_pictures():
    here = (Path(__file__).resolve()).parent
    with tarfile.open(here / "pict_tools_test.tar.xz") as tar:
        tar.extractall(path=tmp_dir)


@pytest.fixture()
def empty_options():
    arguments = docopt(
        pict_tools.pict_build_events_keyword.__doc__,
        argv=[
            "unused",
        ],
    )
    options = dotdict(
        {
            key[2:].replace("-", "_"): value
            for key, value in arguments.items()
            if key.startswith("--")
        }
    )
    yield (options)


def files_in_dir_are(directory, expected_files_list):
    expected_files_list_as_strings = sorted(
        [str(expected_file) for expected_file in expected_files_list]
    )
    recursive_directory_files = sorted(glob(f"{directory}/**/*.*", recursive=True))
    if expected_files_list_as_strings == recursive_directory_files:
        return True
    else:
        print("files_in_dir_are error:")
        print("in", directory, "directory:")
        print("expected files list:", expected_files_list_as_strings)
        print("actual files list:", recursive_directory_files)
        return False


def keywords_are(file_keyword_list):
    for file, keywords in file_keyword_list:
        metadata = pyexiv2.Image(str(file))
        iptc_tags = metadata.read_iptc()
        if "Iptc.Application2.Keywords" in iptc_tags:
            iptc_keywords = metadata.read_iptc()["Iptc.Application2.Keywords"]
            xmp_keywords = metadata.read_xmp()["Xmp.iptc.Keywords"]
        else:
            iptc_keywords = []
            xmp_keywords = []
        if not (
            sorted(iptc_keywords) == sorted(xmp_keywords)
            and sorted(xmp_keywords) == sorted(keywords)
        ):
            print("keywords_are error:")
            print("expected keywords:", keywords)
            print("actual iptc keywords:", iptc_keywords)
            print("actual xmp keywords:", xmp_keywords)
            return False
    return True


def create_dates_are(file_create_date_list):
    with ExifTool() as et:
        for file, create_date in file_create_date_list:
            actual_create_date = et.get_tag("CreateDate", str(file))
            if not (create_date == actual_create_date):
                print("create_dates_are error:")
                print("expected create date:", create_date)
                print("actual iptc keywords:", actual_create_date)
                return False
    return True


def set_create_date(file, create_date):
    with ExifTool() as et:
        et.set_tag(f"CreateDate={create_date}", str(file))


def test_e2e_build_events_keyword_events_already_present(
    init_and_cleanup_1_test_picture, empty_options
):
    expected_file_1 = test_picts_dir / "2000" / "01-01_spring_break" / "test_file_1.jpg"
    expected_file_1.parent.mkdir(parents=True)
    test_file_1.rename(expected_file_1)
    check_and_set_keyword(str(expected_file_1), "events:holiday_in_paris")

    _pict_build_events_keyword(expected_file_1.parent, empty_options)

    assert keywords_are(
        [
            (
                expected_file_1,
                ["events:holiday_in_paris"],
            ),
        ]
    )


def test_e2e_build_events_keyword(init_and_cleanup_1_test_picture, empty_options):
    expected_file_1 = test_picts_dir / "2000" / "01-01_spring_break" / "test_file_1.jpg"
    expected_file_2 = test_picts_dir / "2000" / "Honey moon" / "test_file_2.jpg"
    expected_file_3 = (
        test_picts_dir / "2000" / "01-01-full-moon-party" / "test_file_3.jpg"
    )
    expected_file_4 = (
        test_picts_dir / "2000" / "01-01 Black moon party" / "test_file_4.jpg"
    )
    expected_file_5 = (
        test_picts_dir / "2000" / "ski week in Barèges 01-01" / "test_file_5.jpg"
    )
    expected_file_6 = (
        test_picts_dir / "2000" / "2512 Jesus birthday" / "test_file_6.jpg"
    )
    expected_file_7 = (
        test_picts_dir / "2000" / "2000 new year's eve" / "test_file_7.jpg"
    )
    expected_files = [
        expected_file_1,
        expected_file_2,
        expected_file_3,
        expected_file_4,
        expected_file_5,
        expected_file_6,
        expected_file_7,
    ]
    for expected_file in expected_files:
        expected_file.parent.mkdir(parents=True)
        copyfile(test_file_1, expected_file)

    options = empty_options
    options.recursive = True

    _pict_build_events_keyword(test_picts_dir / "2000", options)

    assert keywords_are(
        [
            (
                expected_file_1,
                ["events:spring_break"],
            ),
            (
                expected_file_2,
                ["events:honey_moon"],
            ),
            (
                expected_file_3,
                ["events:full_moon_party"],
            ),
            (
                expected_file_4,
                ["events:black_moon_party"],
            ),
            (
                expected_file_5,
                ["events:ski_week_in_barèges"],
            ),
            (
                expected_file_6,
                ["events:jesus_birthday"],
            ),
            (
                expected_file_7,
                ["events:new_year_s_eve"],
            ),
        ]
    )
