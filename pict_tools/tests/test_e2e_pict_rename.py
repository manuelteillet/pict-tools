from pict_tools.pict_rename import _pict_rename, image_hash
import pict_tools
from pict_tools.pict_keyword_toggle import check_and_set_keyword
from pict_tools.tools import dotdict
from pict_tools.exiftool import ExifTool
import tarfile

import pytest
from shutil import rmtree, move, copytree, copyfile
from sh import pict_rename, pict_album
from glob import glob
from termcolor import colored
from tempfile import mkdtemp
from pathlib import PurePath
import pyexiv2
from pathlib import Path
from docopt import docopt

tmp_dir = Path(mkdtemp())
test_picts_dir = tmp_dir / "pict_tools_test"
test_dest_dir = tmp_dir / "pict_tools_test_destination"
test_file_1 = test_picts_dir / "test_file_1.jpg"
test_file_2 = test_picts_dir / "test_file_2.jpg"


@pytest.fixture()
def init_and_cleanup_2_test_pictures():
    untar_test_pictures()
    with ExifTool() as et:
        et.set_tag("CreateDate=2000:01:01 12:00:00", str(test_file_1))
        et.set_tag("Make=Google", str(test_file_1))
        et.set_tag("Model=Pixel 5", str(test_file_1))
        et.set_tag("CreateDate=2000:01:01 13:00:00", str(test_file_2))
        et.set_tag("Make=Canon", str(test_file_2))
        et.set_tag("Model=750D", str(test_file_2))

    yield ()

    rmtree(tmp_dir, ignore_errors=True)


@pytest.fixture()
def init_and_cleanup_1_test_picture():
    untar_test_pictures()
    with ExifTool() as et:
        et.set_tag("CreateDate=2000:01:01 12:00:00", str(test_file_1))
        et.set_tag("Make=Google", str(test_file_1))
        et.set_tag("Model=Pixel 5", str(test_file_1))
    test_file_2.unlink()

    yield ()

    rmtree(tmp_dir, ignore_errors=True)


@pytest.fixture()
def init_and_cleanup_2_test_pictures_without_camera_id():
    untar_test_pictures()
    with ExifTool() as et:
        et.set_tag("CreateDate=2000:01:01 12:00:00", str(test_file_1))
        et.set_tag("CreateDate=2000:01:01 13:00:00", str(test_file_2))

    yield ()

    rmtree(tmp_dir, ignore_errors=True)


@pytest.fixture()
def init_and_cleanup_2_test_pictures_with_mixed_camera_ids():
    untar_test_pictures()
    with ExifTool() as et:
        et.set_tag("CreateDate=2000:01:01 12:00:00", str(test_file_1))
        et.set_tag("Make=Google", str(test_file_1))
        et.set_tag("Model=Pixel 5", str(test_file_1))
        et.set_tag("CreateDate=2000:01:01 13:00:00", str(test_file_2))

    yield ()

    rmtree(tmp_dir, ignore_errors=True)


def untar_test_pictures():
    here = (Path(__file__).resolve()).parent
    with tarfile.open(here / "pict_tools_test.tar.xz") as tar:
        tar.extractall(path=tmp_dir)


@pytest.fixture()
def empty_options():
    arguments = docopt(
        pict_tools.pict_rename.__doc__,
        argv=[
            "unused",
        ],
    )
    options = dotdict(
        {
            key[2:].replace("-", "_"): value
            for key, value in arguments.items()
            if key.startswith("--")
        }
    )
    yield (options)


def files_in_dir_are(directory, expected_files_list):
    expected_files_list_as_strings = sorted(
        [str(expected_file) for expected_file in expected_files_list]
    )
    recursive_directory_files = sorted(glob(f"{directory}/**/*.*", recursive=True))
    if expected_files_list_as_strings == recursive_directory_files:
        return True
    else:
        print("files_in_dir_are error:")
        print("in", directory, "directory:")
        print("expected files list:", expected_files_list_as_strings)
        print("actual files list:", recursive_directory_files)
        return False


def keywords_are(file_keyword_list):
    for file, keywords in file_keyword_list:
        metadata = pyexiv2.Image(str(file))
        iptc_tags = metadata.read_iptc()
        if "Iptc.Application2.Keywords" in iptc_tags:
            iptc_keywords = metadata.read_iptc()["Iptc.Application2.Keywords"]
            xmp_keywords = metadata.read_xmp()["Xmp.iptc.Keywords"]
        else:
            iptc_keywords = []
            xmp_keywords = []
        if not (
            sorted(iptc_keywords) == sorted(xmp_keywords)
            and sorted(xmp_keywords) == sorted(keywords)
        ):
            print("keywords_are error:")
            print("expected keywords:", keywords)
            print("actual iptc keywords:", iptc_keywords)
            print("actual xmp keywords:", xmp_keywords)
            return False
    return True


def create_dates_are(file_create_date_list):
    with ExifTool() as et:
        for file, create_date in file_create_date_list:
            actual_create_date = et.get_tag("CreateDate", str(file))
            if not (create_date == actual_create_date):
                print("create_dates_are error:")
                print("expected create date:", create_date)
                print("actual iptc keywords:", actual_create_date)
                return False
    return True


def set_create_date(file, create_date):
    with ExifTool() as et:
        et.set_tag(f"CreateDate={create_date}", str(file))


def test_e2e_rename_2_files(init_and_cleanup_2_test_pictures, empty_options):
    _pict_rename(test_picts_dir, empty_options)

    expected_file_1 = test_picts_dir / "2000-01-01_12-00-00_00_google-pixel-5.jpg"
    expected_file_2 = test_picts_dir / "2000-01-01_13-00-00_00_canon-750d.jpg"
    assert files_in_dir_are(
        test_picts_dir,
        [expected_file_1, expected_file_2],
    )
    assert keywords_are(
        [
            (expected_file_1, ["camera:google-pixel-5"]),
            (expected_file_2, ["camera:canon-750d"]),
        ]
    )


def test_e2e_rename_no_create_date(
    init_and_cleanup_1_test_picture, empty_options, capsys
):
    set_create_date(test_file_1, "")

    _pict_rename(test_picts_dir, empty_options)

    captured = capsys.readouterr()

    assert (
        f"Impossible to rename file {test_file_1} because of bad create date."
        in captured.out
    )


def test_e2e_rename_rebuild_create_date(init_and_cleanup_1_test_picture, empty_options):
    set_create_date(test_file_1, "")
    test_file_1.rename(test_picts_dir / "2000-01-01_12-00-05.jpg")

    _pict_rename(test_picts_dir, empty_options)

    expected_file_1 = test_picts_dir / "2000-01-01_12-00-05_00_google-pixel-5.jpg"
    assert files_in_dir_are(
        test_picts_dir,
        [expected_file_1],
    )
    assert create_dates_are([(expected_file_1, "2000:01:01 12:00:05")])
    assert keywords_are(
        [
            (expected_file_1, ["camera:google-pixel-5"]),
        ]
    )


def test_e2e_rename_and_move_2_files(init_and_cleanup_2_test_pictures, empty_options):
    options = empty_options
    options.move = test_dest_dir

    _pict_rename(test_picts_dir, options)

    expected_file_1 = (
        test_dest_dir / "2000" / "01" / "2000-01-01_12-00-00_00_google-pixel-5.jpg"
    )
    expected_file_2 = (
        test_dest_dir / "2000" / "01" / "2000-01-01_13-00-00_00_canon-750d.jpg"
    )
    assert files_in_dir_are(
        test_picts_dir,
        [],
    )
    assert files_in_dir_are(
        test_dest_dir,
        [expected_file_1, expected_file_2],
    )
    assert keywords_are(
        [
            (expected_file_1, ["camera:google-pixel-5"]),
            (expected_file_2, ["camera:canon-750d"]),
        ]
    )


def test_e2e_rename_and_move_2_files_with_same_create_date(
    init_and_cleanup_2_test_pictures, empty_options
):
    set_create_date(test_file_1, "2000:01:01 13:00:00")
    options = empty_options
    options.move = test_dest_dir

    _pict_rename(test_picts_dir, options)

    _pict_rename(test_picts_dir, options)

    expected_file_1 = (
        test_dest_dir / "2000" / "01" / "2000-01-01_13-00-00_00_google-pixel-5.jpg"
    )
    expected_file_2 = (
        test_dest_dir / "2000" / "01" / "2000-01-01_13-00-00_01_canon-750d.jpg"
    )
    assert files_in_dir_are(
        test_picts_dir,
        [],
    )
    assert files_in_dir_are(
        test_dest_dir,
        [expected_file_1, expected_file_2],
    )
    assert keywords_are(
        [
            (expected_file_1, ["camera:google-pixel-5"]),
            (expected_file_2, ["camera:canon-750d"]),
        ]
    )


def test_e2e_rename_and_move_2_identical_files_without_deleting(
    init_and_cleanup_1_test_picture, empty_options, capsys
):
    copyfile(test_file_1, test_file_2)
    options = empty_options
    options.move = test_dest_dir
    options.no_delete = True

    _pict_rename(test_picts_dir, options)

    expected_file_1 = (
        test_dest_dir / "2000" / "01" / "2000-01-01_12-00-00_00_google-pixel-5.jpg"
    )
    expected_file_2 = (
        test_dest_dir / "2000" / "01" / "2000-01-01_12-00-00_01_google-pixel-5.jpg"
    )
    assert files_in_dir_are(
        test_picts_dir,
        [],
    )
    assert files_in_dir_are(
        test_dest_dir,
        [expected_file_1, expected_file_2],
    )
    assert keywords_are(
        [
            (expected_file_1, ["camera:google-pixel-5"]),
            (expected_file_2, ["camera:google-pixel-5"]),
        ]
    )
    captured = capsys.readouterr()
    assert (
        f"{expected_file_2} is an exact duplicate of {expected_file_1}." in captured.out
    )


def test_e2e_rename_and_move_2_identical_files_with_deleting(
    init_and_cleanup_1_test_picture, empty_options
):
    copyfile(test_file_1, test_file_2)

    options = empty_options
    options.move = test_dest_dir

    _pict_rename(test_picts_dir, options)

    expected_file_1 = (
        test_dest_dir / "2000" / "01" / "2000-01-01_12-00-00_00_google-pixel-5.jpg"
    )
    assert files_in_dir_are(
        test_picts_dir,
        [],
    )
    assert files_in_dir_are(
        test_dest_dir,
        [expected_file_1],
    )
    assert keywords_are(
        [
            (expected_file_1, ["camera:google-pixel-5"]),
        ]
    )


def test_e2e_rename_and_move_2_identical_files_except_metadata(
    init_and_cleanup_1_test_picture,
    empty_options,
    # capsys
):
    copyfile(test_file_1, test_file_2)
    check_and_set_keyword(str(test_file_1), "album1:spring_break")
    options = empty_options
    options.move = test_dest_dir
    options.no_delete = True

    _pict_rename(test_picts_dir, options)

    expected_file_1 = (
        test_dest_dir / "2000" / "01" / "2000-01-01_12-00-00_00_google-pixel-5.jpg"
    )
    expected_file_2 = (
        test_dest_dir / "2000" / "01" / "2000-01-01_12-00-00_01_google-pixel-5.jpg"
    )
    assert files_in_dir_are(
        test_picts_dir,
        [],
    )
    assert files_in_dir_are(
        test_dest_dir,
        [expected_file_1, expected_file_2],
    )
    assert keywords_are(
        [
            (
                expected_file_1,
                ["album1:spring_break", "camera:google-pixel-5"],
            ),
            (expected_file_2, ["camera:google-pixel-5"]),
        ]
    )
    # captured = capsys.readouterr()
    # assert (
    #     f"{expected_file_2} is a duplicate except for tags of {expected_file_1}."
    #     in captured.out
    # )
    assert False


def test_e2e_rename_2_files_with_conflicting_names(
    init_and_cleanup_2_test_pictures_without_camera_id, empty_options
):
    image_hash_file_1 = image_hash(test_file_1)
    image_hash_file_2 = image_hash(test_file_2)
    set_create_date(test_file_1, "2000:01:01 13:00:00")
    expected_file_1 = test_picts_dir / "2000-01-01_13-00-00_00.jpg"
    test_file_1.rename(test_picts_dir / "1.jpg")
    test_file_2.rename(expected_file_1)

    _pict_rename(test_picts_dir, empty_options)

    expected_file_2 = test_picts_dir / "2000-01-01_13-00-00_01.jpg"
    assert files_in_dir_are(
        test_picts_dir,
        [expected_file_1, expected_file_2],
    )
    assert keywords_are(
        [
            (expected_file_1, []),
            (expected_file_2, []),
        ]
    )
    assert image_hash_file_1 == image_hash(expected_file_1)
    assert image_hash_file_2 == image_hash(expected_file_2)


def test_e2e_rename_and_move_1_file_with_conflicting_name(
    init_and_cleanup_2_test_pictures_without_camera_id, empty_options
):
    image_hash_file_1 = image_hash(test_file_1)
    image_hash_file_2 = image_hash(test_file_2)
    set_create_date(test_file_1, "2000:01:01 13:00:00")
    expected_file_1 = test_dest_dir / "2000" / "01" / "2000-01-01_13-00-00_00.jpg"
    expected_file_1.parent.mkdir(parents=True)
    test_file_2.rename(expected_file_1)
    options = empty_options
    options.move = test_dest_dir

    _pict_rename(test_picts_dir, options)

    expected_file_2 = test_dest_dir / "2000" / "01" / "2000-01-01_13-00-00_01.jpg"

    assert files_in_dir_are(
        test_picts_dir,
        [],
    )
    assert files_in_dir_are(
        test_dest_dir,
        [expected_file_1, expected_file_2],
    )
    assert keywords_are(
        [
            (expected_file_1, []),
            (expected_file_2, []),
        ]
    )
    assert image_hash_file_1 == image_hash(expected_file_2)
    assert image_hash_file_2 == image_hash(expected_file_1)


def test_e2e_rename_2_files_with_dup_in_name(
    init_and_cleanup_2_test_pictures_without_camera_id, empty_options
):
    set_create_date(test_file_1, "2000:01:01 13:00:00")
    expected_file_1 = test_dest_dir / "2000" / "01" / "2000-01-01_13-00-00_00.jpg"
    expected_file_1.parent.mkdir(parents=True)
    test_file_2.rename(expected_file_1)
    test_file_1.rename(test_dest_dir / "2000" / "01" / "2000-01-01_13-00-00_00_dup.jpg")
    options = empty_options
    options.recursive = True

    _pict_rename(test_dest_dir, options)

    expected_file_2 = test_dest_dir / "2000" / "01" / "2000-01-01_13-00-00_01.jpg"
    assert files_in_dir_are(
        test_dest_dir,
        [expected_file_1, expected_file_2],
    )


def test_e2e_rename_2_files_with_offset(
    init_and_cleanup_2_test_pictures, empty_options
):
    options = empty_options
    options.offset = "+2:30"

    _pict_rename(test_picts_dir, options)

    expected_file_1 = test_picts_dir / "2000-01-01_14-30-00_00_google-pixel-5.jpg"
    expected_file_2 = test_picts_dir / "2000-01-01_15-30-00_00_canon-750d.jpg"
    assert files_in_dir_are(
        test_picts_dir,
        [expected_file_1, expected_file_2],
    )
    assert create_dates_are(
        [
            (expected_file_1, "2000:01:01 14:30:00"),
            (expected_file_2, "2000:01:01 15:30:00"),
        ]
    )


def test_e2e_rename_2_files_with_offset_and_dry_run(
    init_and_cleanup_2_test_pictures, empty_options, capsys
):
    options = empty_options
    options.offset = "+2:30"
    options.dry_run = True

    _pict_rename(test_picts_dir, options)

    assert files_in_dir_are(
        test_picts_dir,
        [test_file_1, test_file_2],
    )
    assert create_dates_are(
        [
            (test_file_1, "2000:01:01 12:00:00"),
            (test_file_2, "2000:01:01 13:00:00"),
        ]
    )

    expected_file_1 = test_picts_dir / "2000-01-01_14-30-00_00_google-pixel-5.jpg"
    expected_file_2 = test_picts_dir / "2000-01-01_15-30-00_00_canon-750d.jpg"

    captured = capsys.readouterr()
    assert f"{test_file_1} -> {expected_file_1}" in captured.out
    assert f"{test_file_2} -> {expected_file_2}" in captured.out


def test_e2e_rename_2_files_with_fill_camera_id(
    init_and_cleanup_2_test_pictures_with_mixed_camera_ids, empty_options
):
    options = empty_options
    options.fill_camera_id = "whatsapp-toto"

    _pict_rename(test_picts_dir, options)

    expected_file_1 = test_picts_dir / "2000-01-01_12-00-00_00_google-pixel-5.jpg"
    expected_file_2 = test_picts_dir / "2000-01-01_13-00-00_00_whatsapp-toto.jpg"
    assert files_in_dir_are(
        test_picts_dir,
        [expected_file_1, expected_file_2],
    )
    assert keywords_are(
        [
            (expected_file_1, ["camera:google-pixel-5"]),
            (expected_file_2, ["camera:whatsapp-toto"]),
        ]
    )


def test_e2e_rename_2_files_with_override_camera_id(
    init_and_cleanup_2_test_pictures_with_mixed_camera_ids, empty_options
):
    options = empty_options
    options.override_camera_id = "whatsapp-toto"

    _pict_rename(test_picts_dir, options)

    expected_file_1 = test_picts_dir / "2000-01-01_12-00-00_00_whatsapp-toto.jpg"
    expected_file_2 = test_picts_dir / "2000-01-01_13-00-00_00_whatsapp-toto.jpg"
    assert files_in_dir_are(
        test_picts_dir,
        [expected_file_1, expected_file_2],
    )
    assert keywords_are(
        [
            (expected_file_1, ["camera:whatsapp-toto"]),
            (expected_file_2, ["camera:whatsapp-toto"]),
        ]
    )
