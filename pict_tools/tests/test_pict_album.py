from glob import glob
from pict_tools.exiftool import ExifTool
import tarfile
from os import path
from shutil import copytree, rmtree
import pytest
from sh import pict_album
from tempfile import mkdtemp


here = path.dirname(path.realpath(__file__))
tmp_dir = mkdtemp()
test_picts_dir = path.join(tmp_dir, "pict_tools_test")
album_test_picts_dir = path.join(test_picts_dir, "pict_tools_test-album")
test_file_1 = path.join(test_picts_dir, "test_file_1.jpg")
test_file_2 = path.join(test_picts_dir, "test_file_2.jpg")
test_file_3 = path.join(test_picts_dir, "test_file_3.jpg")
test_file_4 = path.join(test_picts_dir, "test_file_4.jpg")
test_file_5 = path.join(test_picts_dir, "test_file_5.jpg")
album_test_file_1 = path.join(album_test_picts_dir, "test_file_1.jpg")
album_test_file_2 = path.join(album_test_picts_dir, "test_file_2.jpg")
album_test_file_3 = path.join(album_test_picts_dir, "test_file_3.jpg")
album_test_file_4 = path.join(album_test_picts_dir, "test_file_4.jpg")
album_test_file_5 = path.join(album_test_picts_dir, "test_file_5.jpg")


@pytest.yield_fixture()
def init_and_delete_test_pictures():
    tar = tarfile.open(path.join(here, "pict_tools_test.tar.xz"))
    tar.extractall(path=tmp_dir)
    tar.close()

    yield ()

    rmtree(tmp_dir, ignore_errors=True)


def test_album_nominal(init_and_delete_test_pictures):
    with ExifTool() as et:
        et.set_tag("Keywords=album", test_file_2)
        et.set_tag("Keywords=test", test_file_3)
        et.set_tag("Keywords=test, album", test_file_5)
    pict_album(test_picts_dir, "album")

    assert sorted(glob(f"{album_test_picts_dir}/*.*")) == [
        album_test_file_2,
        album_test_file_5,
    ]


def test_album_do_not_create_empty_albums(init_and_delete_test_pictures):
    with ExifTool() as et:
        et.set_tag("Keywords=test", test_file_3)
    pict_album(test_picts_dir, "album")

    assert not path.exists(album_test_picts_dir)


def test_album_do_not_create_albums_from_links(init_and_delete_test_pictures):
    with ExifTool() as et:
        et.set_tag("Keywords=album", test_file_2)
        et.set_tag("Keywords=album", test_file_5)
    pict_album(test_picts_dir, "album")
    pict_album(album_test_picts_dir, "album")

    assert not path.exists(
        path.join(album_test_picts_dir, f"{path.basename(album_test_picts_dir)}-album")
    )


def test_album_multiple_directories(init_and_delete_test_pictures):
    test_picts_multiple_dir = path.join(tmp_dir, "pict_tools_multiple_test")
    test_picts_event_1_dir = path.join(test_picts_multiple_dir, "event_1")
    test_picts_event_2_dir = path.join(test_picts_multiple_dir, "event_2")
    directory_list = [
        test_picts_event_2_dir,
        test_picts_event_1_dir,
        test_picts_multiple_dir,
    ]

    copytree(test_picts_dir, test_picts_multiple_dir)
    copytree(test_picts_dir, test_picts_event_1_dir)
    copytree(test_picts_dir, test_picts_event_2_dir)

    with ExifTool() as et:
        for directory in directory_list:
            et.set_tag(
                "Keywords=album",
                path.join(directory, path.relpath(test_file_2, test_picts_dir)),
            )
            et.set_tag(
                "Keywords=test, album",
                path.join(directory, path.relpath(test_file_5, test_picts_dir)),
            )

    pict_album(test_picts_multiple_dir, "album", recursive=True)

    for directory in directory_list:
        album_test_picts_dir = path.join(directory, f"{path.basename(directory)}-album")
        album_test_pict_2 = path.join(
            album_test_picts_dir, path.relpath(test_file_2, test_picts_dir)
        )
        album_test_pict_5 = path.join(
            album_test_picts_dir, path.relpath(test_file_5, test_picts_dir)
        )
        assert sorted(glob(f"{album_test_picts_dir}/*.*")) == [
            album_test_pict_2,
            album_test_pict_5,
        ]


def test_album_multiple_directories_parent_empty(init_and_delete_test_pictures):
    test_picts_multiple_dir = path.join(tmp_dir, "pict_tools_multiple_test")
    test_picts_event_1_dir = path.join(test_picts_multiple_dir, "event_1")
    test_picts_event_2_dir = path.join(test_picts_multiple_dir, "event_2")
    directory_list = [test_picts_event_2_dir, test_picts_event_1_dir]

    copytree(test_picts_dir, test_picts_event_1_dir)
    copytree(test_picts_dir, test_picts_event_2_dir)

    with ExifTool() as et:
        for directory in directory_list:
            et.set_tag(
                "Keywords=album",
                path.join(directory, path.relpath(test_file_2, test_picts_dir)),
            )
            et.set_tag(
                "Keywords=test, album",
                path.join(directory, path.relpath(test_file_5, test_picts_dir)),
            )

    pict_album(test_picts_multiple_dir, "album", recursive=True)

    album_test_picts_dir = path.join(
        test_picts_multiple_dir, f"{path.basename(test_picts_multiple_dir)}-album"
    )
    assert not path.exists(album_test_picts_dir)

    for directory in directory_list:
        album_test_picts_dir = path.join(directory, f"{path.basename(directory)}-album")
        album_test_pict_2 = path.join(
            album_test_picts_dir, path.relpath(test_file_2, test_picts_dir)
        )
        album_test_pict_5 = path.join(
            album_test_picts_dir, path.relpath(test_file_5, test_picts_dir)
        )
        assert sorted(glob(f"{album_test_picts_dir}/*.*")) == [
            album_test_pict_2,
            album_test_pict_5,
        ]


def test_album_multiple_directories_without_recursive_flag(
    init_and_delete_test_pictures,
):
    test_picts_multiple_dir = path.join(tmp_dir, "pict_tools_multiple_test")
    test_picts_event_1_dir = path.join(test_picts_multiple_dir, "event_1")
    test_picts_event_2_dir = path.join(test_picts_multiple_dir, "event_2")
    directory_list = [
        test_picts_event_2_dir,
        test_picts_event_1_dir,
        test_picts_multiple_dir,
    ]

    copytree(test_picts_dir, test_picts_multiple_dir)
    copytree(test_picts_dir, test_picts_event_1_dir)
    copytree(test_picts_dir, test_picts_event_2_dir)

    with ExifTool() as et:
        for directory in directory_list:
            et.set_tag(
                "Keywords=album",
                path.join(directory, path.relpath(test_file_2, test_picts_dir)),
            )
            et.set_tag(
                "Keywords=test, album",
                path.join(directory, path.relpath(test_file_5, test_picts_dir)),
            )

    pict_album(test_picts_multiple_dir, "album")

    album_test_picts_dir = path.join(
        test_picts_multiple_dir, f"{path.basename(test_picts_multiple_dir)}-album"
    )
    album_test_pict_2 = path.join(
        album_test_picts_dir, path.relpath(test_file_2, test_picts_dir)
    )
    album_test_pict_5 = path.join(
        album_test_picts_dir, path.relpath(test_file_5, test_picts_dir)
    )
    assert sorted(glob(f"{album_test_picts_dir}/*.*")) == [
        album_test_pict_2,
        album_test_pict_5,
    ]

    for directory in directory_list[:-1]:
        album_test_picts_dir = path.join(directory, f"{path.basename(directory)}-album")
        assert sorted(glob(f"{album_test_picts_dir}/*.*")) == []
