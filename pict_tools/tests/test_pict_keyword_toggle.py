from pict_tools.exiftool import ExifTool
import tarfile
from os import path
import pytest
from shutil import rmtree
from sh import pict_keyword_toggle


here = path.dirname(path.realpath(__file__))
test_picts_dir = path.join(here, "pict_tools_test")
test_file_1 = path.join(test_picts_dir, "test_file_1.jpg")
test_file_2 = path.join(test_picts_dir, "test_file_2.jpg")
test_file_3 = path.join(test_picts_dir, "test_file_3.jpg")
test_file_4 = path.join(test_picts_dir, "test_file_4.jpg")
test_file_5 = path.join(test_picts_dir, "test_file_5.jpg")
test_files_list = [test_file_1, test_file_2, test_file_3, test_file_4, test_file_5]


@pytest.yield_fixture()
def init_and_delete_test_pictures():
    tar = tarfile.open(path.join(here, "pict_tools_test.tar.xz"))
    tar.extractall(path=here)
    tar.close()

    yield ()

    rmtree(test_picts_dir, ignore_errors=True)


def test_keyword_toggle_2_keywords(init_and_delete_test_pictures):
    with ExifTool() as et:
        for test_file in test_files_list:
            assert et.get_tag("Keywords", test_file) is None
        pict_keyword_toggle("test1", test_files_list)
        for test_file in test_files_list:
            assert et.get_tag("Keywords", test_file) == "test1"
        pict_keyword_toggle("test2", test_files_list)
        for test_file in test_files_list:
            assert et.get_tag("Keywords", test_file) == "test1, test2"
        pict_keyword_toggle("test1", test_files_list)
        for test_file in test_files_list:
            assert et.get_tag("Keywords", test_file) == "test2"
        pict_keyword_toggle("test2", test_files_list)
        for test_file in test_files_list:
            assert et.get_tag("Keywords", test_file) is None
