from pict_tools.tools import dotdict
from pict_tools.pict_rename import (
    build_new_file_name,
    build_camera_id,
    build_destination_directory,
    is_create_date_valid,
)


def test_build_destination_directory():
    assert (
        build_destination_directory(
            dotdict({"move": "home/directory"}), "2000:01:01 12:00:00"
        )
        == "home/directory/2000/01"
    )


def test_build_camera_id():
    camera_id = build_camera_id(None, None)
    assert camera_id == ""
    camera_id = build_camera_id("", "")
    assert camera_id == ""
    camera_id = build_camera_id("Google", "Google Pixel 5")
    assert camera_id == "google-pixel-5"
    camera_id = build_camera_id("Google", "Pixel 5")
    assert camera_id == "google-pixel-5"
    camera_id = build_camera_id("Google", None)
    assert camera_id == "google"
    camera_id = build_camera_id(None, "Pixel 5")
    assert camera_id == "pixel-5"


def test_build_new_file_name():
    new_file_name = build_new_file_name(
        "test_file.jpg",
        {"create_date": "2000:01:01 12:00:00", "camera_id": "google-pixel-5"},
        dotdict({"move": None}),
        2,
    )
    assert new_file_name == "2000-01-01_12-00-00_02_google-pixel-5.jpg"


def test_build_new_file_name_already_renamed():
    new_file_name = build_new_file_name(
        "2000-01-01_12-00-00.jpg",
        {"create_date": "2000:01:01 12:00:00", "camera_id": "google-pixel-5"},
        dotdict({"move": None}),
        2,
    )
    assert new_file_name == "2000-01-01_12-00-00_02_google-pixel-5.jpg"

    new_file_name = build_new_file_name(
        "2000-01-01_12-00-00_edit1.jpg",
        {"create_date": "2000:01:01 12:00:00", "camera_id": "google-pixel-5"},
        dotdict({"move": None}),
        2,
    )
    assert new_file_name == "2000-01-01_12-00-00_02_google-pixel-5_edit1.jpg"

    new_file_name = build_new_file_name(
        "2000-01-01_12-00-00-001_edit2.jpg",
        {"create_date": "2000:01:01 12:00:00", "camera_id": "google-pixel-5"},
        dotdict({"move": None}),
        2,
    )
    assert new_file_name == "2000-01-01_12-00-00_02_google-pixel-5_edit2.jpg"

    new_file_name = build_new_file_name(
        "2000-01-01_12-00-00_02_google-pixel-5_edit3.jpg",
        {"create_date": "2000:01:01 12:00:00", "camera_id": "google-pixel-5"},
        dotdict({"move": None}),
        2,
    )
    assert new_file_name == "2000-01-01_12-00-00_02_google-pixel-5_edit3.jpg"

    new_file_name = build_new_file_name(
        "2000-01-01_12-00-00_dup.jpg",
        {"create_date": "2000:01:01 12:00:00", "camera_id": ""},
        dotdict({"move": None}),
        2,
    )
    assert new_file_name == "2000-01-01_12-00-00_02.jpg"


def test_build_new_file_name_with_new_path():
    new_file_name = build_new_file_name(
        "/old/path/2000-01-01_12-00-00.jpg",
        {"create_date": "2000:01:01 12:00:00", "camera_id": "google-pixel-5"},
        dotdict({"move": None}),
        2,
    )
    assert new_file_name == "/old/path/2000-01-01_12-00-00_02_google-pixel-5.jpg"
    new_file_name = build_new_file_name(
        "/old/path/2000-01-01_12-00-00.jpg",
        {"create_date": "2000:01:01 12:00:00", "camera_id": "google-pixel-5"},
        dotdict({"move": "/new/path"}),
        2,
    )
    assert (
        new_file_name == "/new/path/2000/01/2000-01-01_12-00-00_02_google-pixel-5.jpg"
    )


def test_is_create_date_valid():
    assert True == is_create_date_valid("2023:10:05 23:55:24")
    assert False == is_create_date_valid(None)
    assert False == is_create_date_valid("0000:00:00 00:00:00")
    assert False == is_create_date_valid("toto")
    assert False == is_create_date_valid("2023:10:05T23:55:24")


# def test_rename_get_files_dict_without_move(init_and_delete_test_pictures):
#     expected_dict = {
#         test_file_1: {
#             "tmp_name": path.join(
#                 test_picts_dir, "tmp_2000-01-01_12-00-00_00_google-pixel-5.jpg"
#             ),
#             "new_name": path.join(
#                 test_picts_dir, "2000-01-01_12-00-00_00_google-pixel-5.jpg"
#             ),
#         },
#         test_file_2: {
#             "tmp_name": path.join(
#                 test_picts_dir, "tmp_2000-01-01_13-00-00_00_canon-750d.jpg"
#             ),
#             "new_name": path.join(
#                 test_picts_dir, "2000-01-01_13-00-00_00_canon-750d.jpg"
#             ),
#         },
#     }
#     assert get_files_dict([test_file_1, test_file_2]) == expected_dict


# def test_rename_get_files_dict_with_move(init_and_delete_test_pictures):
#     new_path = "/new/path/2000/01"
#     expected_dict = {
#         test_file_1: {
#             "tmp_name": path.join(
#                 new_path, "tmp_2000-01-01_12-00-00_00_google-pixel-5.jpg"
#             ),
#             "new_name": path.join(
#                 new_path, "2000-01-01_12-00-00_00_google-pixel-5.jpg"
#             ),
#         },
#         test_file_2: {
#             "tmp_name": path.join(
#                 new_path, "tmp_2000-01-01_13-00-00_00_canon-750d.jpg"
#             ),
#             "new_name": path.join(new_path, "2000-01-01_13-00-00_00_canon-750d.jpg"),
#         },
#     }
#     assert (
#         get_files_dict([test_file_1, test_file_2], True, "/new/path") == expected_dict
#     )


# def test_rename_get_files_dict_duplicate_file_with_move(init_and_delete_test_pictures):
#     # test_file_1_1 = path.join(test_picts_dir, "test_file_1_1.jpg")
#     # copyfile(test_file_1, test_file_1_1)
#     new_path = "/new/path/2000/01"

#     expected_dict = {
#         test_file_1: {
#             "tmp_name": path.join(
#                 new_path, "tmp_2000-01-01_12-00-00_00_google-pixel-5.jpg"
#             ),
#             "new_name": path.join(
#                 new_path, "2000-01-01_12-00-00_00_google-pixel-5.jpg"
#             ),
#         },
#         test_file_1: {
#             "tmp_name": path.join(
#                 new_path, "tmp_2000-01-01_12-00-00_01_google-pixel-5.jpg"
#             ),
#             "new_name": path.join(
#                 new_path, "2000-01-01_12-00-00_01_google-pixel-5.jpg"
#             ),
#         },
#     }
#     assert (
#         get_files_dict([test_file_1, test_file_1], True, "/new/path") == expected_dict
#     )


# def test_rename_get_files_dict_file_already_in_destination_with_move(
#     init_and_delete_test_pictures,
# ):
#     destination_path = path.join(test_picts_dir, "2000", "01")
#     test_file_1_1 = path.join(
#         destination_path, "2000-01-01_12-00-00_00_google-pixel-5.jpg"
#     )
#     makedirs(destination_path)
#     copyfile(test_file_1, test_file_1_1)

#     expected_dict = {
#         test_file_1: {
#             "tmp_name": path.join(
#                 destination_path, "tmp_2000-01-01_12-00-00_01_google-pixel-5.jpg"
#             ),
#             "new_name": path.join(
#                 destination_path, "2000-01-01_12-00-00_01_google-pixel-5.jpg"
#             ),
#         },
#     }
#     assert get_files_dict([test_file_1], True, test_picts_dir) == expected_dict
