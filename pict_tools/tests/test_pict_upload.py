from glob import glob
from pict_tools.exiftool import ExifTool
import tarfile
from os import path, environ
import pytest
from shutil import rmtree, copytree
from sh import (
    pict_upload,
    pict_album,
    mkdir,
    ssh_agent,
    ssh_keyscan,
    ssh_keygen,
    docker,
    chmod,
    rm,
)
from tempfile import mkdtemp
import time
import re


here = path.dirname(path.realpath(__file__))
tmp_dir = mkdtemp()

test_picts_dir = "pict_tools_test"
test_picts_dir_full_path = path.join(tmp_dir, test_picts_dir)
test_file_1 = "test_file_1.jpg"
test_file_2 = "test_file_2.jpg"
test_file_3 = "test_file_3.jpg"
test_file_4 = "test_file_4.jpg"
test_file_5 = "test_file_5.jpg"
test_files_list = [test_file_1, test_file_2, test_file_3, test_file_4, test_file_5]
test_file_2_full_path = path.join(test_picts_dir_full_path, test_file_2)
test_file_5_full_path = path.join(test_picts_dir_full_path, test_file_5)

remote_picts_dir_full_path = path.join(tmp_dir, "remote_pict_tools_test")


@pytest.yield_fixture()
def init_and_delete_test_pictures():
    tar = tarfile.open(path.join(here, "pict_tools_test.tar.xz"))
    tar.extractall(path=tmp_dir)
    tar.close()

    yield ()

    rmtree(tmp_dir, ignore_errors=True)


@pytest.yield_fixture()
def remote_rsync_server():
    if "rsync_server_ip_address" in environ:
        ip_address = environ["rsync_server_ip_address"]
        docker_already_started = True
    else:
        docker_already_started = False

    if not docker_already_started:
        test_server_container = str(
            docker.run(
                "-d",
                "-e",
                "SSH_USERS=test:1000:1000,http:33:33",
                "docker.io/panubo/sshd",
            )
        )[:-1]

        docker.cp(
            f'{path.join(here, "test_rsa_key.pub")}',
            f"{test_server_container}:/root/.ssh/authorized_keys",
        )
        docker.cp(
            f'{path.join(here, "test_rsa_key.pub")}',
            f"{test_server_container}:/etc/authorized_keys/test",
        )

        test_server_container_attrs = str(docker.inspect(test_server_container))
        ip_address = re.search(
            r'"IPAddress": "([\d.]+)', test_server_container_attrs
        ).group(1)

    timeout = time.time() + 10
    while time.time() < timeout:
        time.sleep(1)
        if ssh_keyscan(ip_address):
            break

    mkdir("-p", path.expanduser("~/.ssh/"))
    with open(path.expanduser("~/.ssh/known_hosts"), "a") as known_hosts:
        ssh_keyscan(ip_address, _out=known_hosts)

    chmod("600", path.join(here, "test_rsa_key"))

    ssh_agent.bash(
        "-c",
        f'''ssh-add {path.join(here, 'test_rsa_key')};
            ssh root@{ip_address} "mkdir -p /mnt/data/external/test/Pictures;
                                   chmod 775 /mnt/data/external/test/Pictures;
                                   chown http:test /mnt/data/external/test/Pictures;
                                   apk --no-cache add shadow;
                                   usermod -aG http test;"''',
    )

    yield (ip_address)

    ssh_keygen("-R", ip_address)
    if not docker_already_started:
        docker.stop(test_server_container)


def test_upload_local(init_and_delete_test_pictures):
    with ExifTool() as et:
        et.set_tag(
            "CreateDate = 2000-01-01T12:00",
            path.join(test_picts_dir_full_path, test_file_1),
        )

        et.set_tag("Keywords=album", test_file_2_full_path)
        et.set_tag("Keywords=album", test_file_5_full_path)

    pict_album(test_picts_dir_full_path, "album")
    pict_upload(test_picts_dir_full_path, remote_picts_dir_full_path)

    expected_remote_file_list = [path.join(remote_picts_dir_full_path, "2000")]
    expected_remote_file_list += [
        path.join(remote_picts_dir_full_path, "2000", test_picts_dir)
    ]
    expected_remote_file_list += [
        path.join(remote_picts_dir_full_path, "2000", test_picts_dir, test_file)
        for test_file in test_files_list
    ]
    expected_remote_file_list += [
        path.join(
            remote_picts_dir_full_path,
            "2000",
            test_picts_dir,
            f"{test_picts_dir}-album",
        )
    ]
    expected_remote_file_list += [
        path.join(
            remote_picts_dir_full_path,
            "2000",
            test_picts_dir,
            f"{test_picts_dir}-album",
            test_file,
        )
        for test_file in [test_file_2, test_file_5]
    ]
    remote_file_list = glob(f"{remote_picts_dir_full_path}/*")
    remote_file_list += glob(f"{remote_picts_dir_full_path}/**/*")
    remote_file_list += glob(f"{remote_picts_dir_full_path}/**/**/*")
    remote_file_list += glob(f"{remote_picts_dir_full_path}/**/**/**/*")
    print(sorted(expected_remote_file_list))
    print(sorted(remote_file_list))
    assert sorted(remote_file_list) == sorted(expected_remote_file_list)


def test_upload_local_multiple_directories(init_and_delete_test_pictures):
    test_picts_multiple_dir_full_path = path.join(tmp_dir, "pict_tools_multiple_test")
    test_picts_event_1_dir_full_path = path.join(
        test_picts_multiple_dir_full_path, "2018", "event_1"
    )
    test_picts_event_2_dir_full_path = path.join(
        test_picts_multiple_dir_full_path, "event_2"
    )

    copytree(test_picts_dir_full_path, test_picts_event_1_dir_full_path)
    copytree(test_picts_dir_full_path, test_picts_event_2_dir_full_path)

    with ExifTool() as et:
        et.set_tag(
            "CreateDate = 2002-01-01T12:00",
            path.join(test_picts_event_1_dir_full_path, test_file_1),
        )

        et.set_tag(
            "Keywords=album", path.join(test_picts_event_1_dir_full_path, test_file_2)
        )
        et.set_tag(
            "Keywords=album", path.join(test_picts_event_1_dir_full_path, test_file_5)
        )

    pict_album(test_picts_event_1_dir_full_path, "album")
    pict_upload(
        test_picts_multiple_dir_full_path, remote_picts_dir_full_path, recursive=True
    )

    expected_remote_file_list = [path.join(remote_picts_dir_full_path, "2000")]
    expected_remote_file_list += [
        path.join(remote_picts_dir_full_path, "2000", "event_2")
    ]
    expected_remote_file_list += [
        path.join(remote_picts_dir_full_path, "2000", "event_2", test_file)
        for test_file in test_files_list
    ]
    expected_remote_file_list += [path.join(remote_picts_dir_full_path, "2002")]
    expected_remote_file_list += [
        path.join(remote_picts_dir_full_path, "2002", "event_1")
    ]
    expected_remote_file_list += [
        path.join(remote_picts_dir_full_path, "2002", "event_1", "event_1-album")
    ]
    expected_remote_file_list += [
        path.join(
            remote_picts_dir_full_path, "2002", "event_1", "event_1-album", test_file
        )
        for test_file in [test_file_2, test_file_5]
    ]
    expected_remote_file_list += [
        path.join(remote_picts_dir_full_path, "2002", "event_1", test_file)
        for test_file in test_files_list
    ]

    remote_file_list = glob(f"{remote_picts_dir_full_path}/*")
    remote_file_list += glob(f"{remote_picts_dir_full_path}/**/*")
    remote_file_list += glob(f"{remote_picts_dir_full_path}/**/**/*")
    remote_file_list += glob(f"{remote_picts_dir_full_path}/**/**/**/*")
    assert sorted(remote_file_list) == sorted(expected_remote_file_list)


def test_upload_remote_rights(init_and_delete_test_pictures, remote_rsync_server):
    with ExifTool() as et:
        et.set_tag("Keywords=album", test_file_2_full_path)
        et.set_tag("Keywords=album", test_file_5_full_path)

    pict_album(test_picts_dir_full_path, "album")
    ssh_agent.bash(
        "-c",
        f"""ssh-add {path.join(here, 'test_rsa_key')}; pict-upload {test_picts_dir_full_path} test@{remote_rsync_server}:/mnt/data/external/test/Pictures""",
        __err_to_out=True,
    )

    check_remote_path(remote_rsync_server, "Pictures", [("d", "2000")])
    check_remote_path(remote_rsync_server, "Pictures/2000", [("d", "pict_tools_test")])
    check_remote_path(
        remote_rsync_server,
        "Pictures/2000/pict_tools_test",
        [
            ("d", "pict_tools_test-album"),
            ("-", "test_file_1.jpg"),
            ("-", "test_file_2.jpg"),
            ("-", "test_file_3.jpg"),
            ("-", "test_file_4.jpg"),
            ("-", "test_file_5.jpg"),
        ],
    )
    check_remote_path(
        remote_rsync_server,
        "Pictures/2000/pict_tools_test/pict_tools_test-album",
        [
            ("l", "test_file_2.jpg -> ../test_file_2.jpg"),
            ("l", "test_file_5.jpg -> ../test_file_5.jpg"),
        ],
    )


def test_upload_remote_delete_no_options(
    init_and_delete_test_pictures, remote_rsync_server
):
    with ExifTool() as et:
        et.set_tag("Keywords=album", test_file_2_full_path)
        et.set_tag("Keywords=album", test_file_5_full_path)

    pict_album(test_picts_dir_full_path, "album")
    ssh_agent.bash(
        "-c",
        f"""ssh-add {path.join(here, 'test_rsa_key')}; pict-upload {test_picts_dir_full_path} test@{remote_rsync_server}:/mnt/data/external/test/Pictures""",
        __err_to_out=True,
    )

    rm(test_file_2_full_path)

    pict_album(test_picts_dir_full_path, "album")
    ssh_agent.bash(
        "-c",
        f"""ssh-add {path.join(here, 'test_rsa_key')}; pict-upload {test_picts_dir_full_path} test@{remote_rsync_server}:/mnt/data/external/test/Pictures""",
        __err_to_out=True,
    )

    check_remote_path(remote_rsync_server, "Pictures", [("d", "2000")])
    check_remote_path(remote_rsync_server, "Pictures/2000", [("d", "pict_tools_test")])
    check_remote_path(
        remote_rsync_server,
        "Pictures/2000/pict_tools_test",
        [
            ("d", "pict_tools_test-album"),
            ("-", "test_file_1.jpg"),
            ("-", "test_file_2.jpg"),
            ("-", "test_file_3.jpg"),
            ("-", "test_file_4.jpg"),
            ("-", "test_file_5.jpg"),
        ],
    )
    check_remote_path(
        remote_rsync_server,
        "Pictures/2000/pict_tools_test/pict_tools_test-album",
        [("l", "test_file_5.jpg -> ../test_file_5.jpg")],
    )


def test_upload_remote_delete_with_delete_option(
    init_and_delete_test_pictures, remote_rsync_server
):
    with ExifTool() as et:
        et.set_tag("Keywords=album", test_file_2_full_path)
        et.set_tag("Keywords=album", test_file_5_full_path)

    pict_album(test_picts_dir_full_path, "album")
    ssh_agent.bash(
        "-c",
        f"""ssh-add {path.join(here, 'test_rsa_key')}; pict-upload {test_picts_dir_full_path} test@{remote_rsync_server}:/mnt/data/external/test/Pictures""",
        __err_to_out=True,
    )

    rm(test_file_2_full_path)

    pict_album(test_picts_dir_full_path, "album")
    ssh_agent.bash(
        "-c",
        f"""ssh-add {path.join(here, 'test_rsa_key')}; pict-upload {test_picts_dir_full_path} --delete-pictures test@{remote_rsync_server}:/mnt/data/external/test/Pictures""",
        __err_to_out=True,
    )

    check_remote_path(remote_rsync_server, "Pictures", [("d", "2000")])
    check_remote_path(remote_rsync_server, "Pictures/2000", [("d", "pict_tools_test")])
    check_remote_path(
        remote_rsync_server,
        "Pictures/2000/pict_tools_test",
        [
            ("d", "pict_tools_test-album"),
            ("-", "test_file_1.jpg"),
            ("-", "test_file_3.jpg"),
            ("-", "test_file_4.jpg"),
            ("-", "test_file_5.jpg"),
        ],
    )
    check_remote_path(
        remote_rsync_server,
        "Pictures/2000/pict_tools_test/pict_tools_test-album",
        [("l", "test_file_5.jpg -> ../test_file_5.jpg")],
    )


def test_upload_remote_delete_with_preserve_option(
    init_and_delete_test_pictures, remote_rsync_server
):
    with ExifTool() as et:
        et.set_tag("Keywords=album", test_file_2_full_path)
        et.set_tag("Keywords=album", test_file_5_full_path)

    pict_album(test_picts_dir_full_path, "album")
    ssh_agent.bash(
        "-c",
        f"""ssh-add {path.join(here, 'test_rsa_key')}; pict-upload {test_picts_dir_full_path} test@{remote_rsync_server}:/mnt/data/external/test/Pictures""",
        __err_to_out=True,
    )

    rm(test_file_2_full_path)

    pict_album(test_picts_dir_full_path, "album")
    ssh_agent.bash(
        "-c",
        f"""ssh-add {path.join(here, 'test_rsa_key')}; pict-upload {test_picts_dir_full_path} --keep-album-links test@{remote_rsync_server}:/mnt/data/external/test/Pictures""",
        __err_to_out=True,
    )

    check_remote_path(remote_rsync_server, "Pictures", [("d", "2000")])
    check_remote_path(remote_rsync_server, "Pictures/2000", [("d", "pict_tools_test")])
    check_remote_path(
        remote_rsync_server,
        "Pictures/2000/pict_tools_test",
        [
            ("d", "pict_tools_test-album"),
            ("-", "test_file_1.jpg"),
            ("-", "test_file_2.jpg"),
            ("-", "test_file_3.jpg"),
            ("-", "test_file_4.jpg"),
            ("-", "test_file_5.jpg"),
        ],
    )
    check_remote_path(
        remote_rsync_server,
        "Pictures/2000/pict_tools_test/pict_tools_test-album",
        [
            ("l", "test_file_2.jpg -> ../test_file_2.jpg"),
            ("l", "test_file_5.jpg -> ../test_file_5.jpg"),
        ],
    )


def check_remote_path(server, remote_path, expected_content):
    remote_path = path.join("/mnt/data/external/test", remote_path)
    content = ssh_agent.bash(
        "-c",
        f'''ssh-add {path.join(here, 'test_rsa_key')}; ssh root@{server} "ls -al {remote_path}"''',
    )
    for i in range(len(expected_content)):
        type, name = expected_content[i]
        rights = "lrwxrwxrwx" if type == "l" else f"{type}rwxrwxr-x"
        assert re.match(
            f"{rights}\s+\d+\s+test\s+http\s+\d+\s+\w+\s+\d+\s+\d+:\d+\s+{name}",
            content.splitlines()[i + 3],
        )
