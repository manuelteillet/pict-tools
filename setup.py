"""Setup file for installing pict-tools."""

from setuptools import setup

setup(
    name="pict-tools",
    version="1.0.0",
    description="A set of small scripts to help me managing my photos.",
    long_description="To see what each tool can do, see --help",
    url="https://gitlab.com/manuelteillet/pict-tools",
    author="Manuel Teillet",
    author_email="manuel.teillet@gmail.com",
    packages=["pict_tools"],
    install_requires=["docopt", "termcolor", "sh", "pyexiv2", "Pillow"],
    extras_require={"test": "tox"},
    entry_points={
        "console_scripts": [
            "pict-rename=pict_tools.pict_rename:main",
            "pict-keyword-toggle=pict_tools.pict_keyword_toggle:main",
            "pict-album=pict_tools.pict_album:main",
            "pict-album-server=pict_tools.pict_album_server:main",
            "pict-upload=pict_tools.pict_upload:main",
        ],
    },
)
